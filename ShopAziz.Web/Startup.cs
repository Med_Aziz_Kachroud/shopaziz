﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ShopAziz.Web.Startup))]
namespace ShopAziz.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
